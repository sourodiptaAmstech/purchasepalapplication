<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['middleware' => ['api']], function () {
Route::get('country','Country\CountryController@country');
Route::get('country/{id}','Country\CountryController@countrybyid');
Route::get('state','Country\CountryController@state');
Route::get('state/{id}','Country\CountryController@statebyid');
Route::get('state/country/{id}','Country\CountryController@statebycountryid');
Route::get('city','Country\CountryController@city');
Route::get('city/{id}','Country\CountryController@citybyid');
Route::get('city/state/{id}','Country\CountryController@citybystateid');
Route::post('registration','Country\CountryController@registration');
Route::post('login','Country\CountryController@login');
Route::get('profile','Country\CountryController@profile');
Route::get('forgetpass','Country\CountryController@forgetpass');
Route::post('forgetpass','Country\CountryController@forgetpassSave');
Route::post('changepassword','Country\CountryController@changepassword');
Route::post('otpveificationemail','Country\CountryController@otpveificationemail');
Route::post('save/bank','Country\CountryController@saveBank');
Route::get('get/bank','Country\CountryController@getBank');



Route::group(['middleware' => 'auth:api'], function () {
 //   Route::get('country','Country\CountryController@country');
});
});
