<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_cities', function (Blueprint $table) {
            $table->bigIncrements('id',10);
			$table->string('name', 30);
			$table->unsignedBigInteger('states_id')->index();
			$table->timestamps();
			$table->softDeletes();
			$table->foreign('states_id')->references('id')->on('_states');
				$table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_cities');
    }
}
