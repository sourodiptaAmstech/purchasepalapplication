<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_countries', function (Blueprint $table) {
            $table->bigIncrements('id',10);
            $table->string('name', 150);
            $table->string('sortname',10);
			$table->integer('phonecode');
            $table->timestamps();
			$table->softDeletes();
				$table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_countries');
    }
}
