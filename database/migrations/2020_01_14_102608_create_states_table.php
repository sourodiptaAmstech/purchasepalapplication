<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_states', function (Blueprint $table) {
            $table->bigIncrements('id',10);
			$table->string('name', 30);
			$table->unsignedBigInteger('country_id')->index();
            $table->timestamps();
			$table->foreign('country_id')->references('id')->on('_countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_states');
    }
}
