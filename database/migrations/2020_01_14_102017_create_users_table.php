<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_users', function (Blueprint $table) {
			
			$table->bigIncrements('user_id', 10);			
			$table->string('user_fname', 100);
			$table->string('user_mname', 100)->nullable();
			$table->string('user_lname', 100);
			$table->string('user_email', 250)->unique();
			$table->string('user_phone', 250)->unique();
			$table->string('user_password', 500);
			$table->string('user_photo', 100)->nullable();
			$table->date('user_dob');
			$table->string('user_address_line_one', 250)->nullable();
			$table->string('user_address_line_two', 250)->nullable();
			$table->unsignedBigInteger('country_id');
			$table->unsignedBigInteger('states_id');
			$table->unsignedBigInteger('city_id');
			$table->string('user_zip_code', 50);							
			$table->integer('user_platform_verification_status');
			$table->string('user_platform_otp', 10);
			$table->integer('user_synapse_status');
			//	$table->timestamps();
			$table->timestamp('user_created_on')->useCurrent();		
			$table->timestamp('user_updated_on')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));;	
			$table->softDeletes();			
			$table->index(['user_email', 'user_password']);
			$table->index(['user_id', 'country_id','states_id','city_id']);
			//$table->foreign('country_id')->references('id')->on('_countries');
			//$table->foreign('states_id')->references('id')->on('_states');
			//$table->foreign('city_id')->references('id')->on('_cities');
			$table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
