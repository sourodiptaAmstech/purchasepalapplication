<?php

namespace App\Http\Controllers\Country;

use App\Cities as AppCities;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Countries;
use App\Model\Cities;
use App\Model\States;
use App\Model\User;
use App\Model\FundingAccount;
use Exception;

class CountryController extends Controller
{

    private function checkValidOtp($email,$otp){
        try{
            $returnData = User::where('user_email', $email)->where('user_platform_otp', $otp)->get();
            $otpValidationResult["txt_status"]="Invalid";
            if(!empty($returnData)){
                if(count($returnData)>0){
                    $otpValidationResult["txt_status"]="OK";
                }
            }
            //     print_r($returnData); exit;
            return $otpValidationResult; //exit;
        }
        catch(Exception $e){

        }
    }
    public function country(Request $request){
        $country=Countries::get();
        if(is_null($country)){
            return response()->json(array("msg"=>"Record Not Found","data"=>[],"status"=>404),404);
        }
        return response()->json(array("msg"=>"Record Found","data"=>$country,"status"=>200),200);
    }
    public function countrybyid(Request $request){
        $id=$request->id;
        $country=Countries::find($id);
        if(is_null($country)){
            return response()->json(array("msg"=>"Record Not Found","data"=>[],"status"=>404),404);
        }
        return response()->json(array("msg"=>"Record Found","data"=>$country,"status"=>200),200);
    }
    public function state(Request $request){
        $state=States::get();
        if(is_null($state)){
            return response()->json(array("msg"=>"Record Not Found","data"=>[],"status"=>404),404);
        }
        return response()->json(array("msg"=>"Record Found","data"=>$state,"status"=>200),200);
    }
    public function statebyid(Request $request){
        $id=$request->id;
        $state=States::find($id);
        if(is_null($state)){
            return response()->json(array("msg"=>"Record Not Found","data"=>[],"status"=>404),404);
        }
        return response()->json(array("msg"=>"Record Found","data"=>$state,"status"=>200),200);
    }
    public function statebycountryid(Request $request){
        $id=$request->id;
        $state=States::with('countries')->find($id);
        if(is_null($state)){
            return response()->json(array("msg"=>"Record Not Found","data"=>[],"status"=>404),404);
        }
        return response()->json(array("msg"=>"Record Found","data"=>$state,"status"=>200),200);
    }
    public function city(Request $request){
        $city=Cities::get();
        if(is_null($city)){
            return response()->json(array("msg"=>"Record Not Found","data"=>[],"status"=>404),404);
        }
        return response()->json(array("msg"=>"Record Found","data"=>$city,"status"=>200),200);
    }
    public function citybyid(Request $request){
        $id=$request->id;
        $city=Cities::find($id);
        if(is_null($city)){
            return response()->json(array("msg"=>"Record Not Found","data"=>[],"status"=>404),404);
        }
        return response()->json(array("msg"=>"Record Found","data"=>$city,"status"=>200),200);
    }
    public function citybystateid(Request $request){
        $id=$request->id;
        $city=Cities::with('states')->find($id);
        if(is_null($city)){
            return response()->json(array("msg"=>"Record Not Found","data"=>[],"status"=>404),404);
        }
        return response()->json(array("msg"=>"Record Found","data"=>$city,"status"=>200),200);
    }
    public function registration(Request $request){
        try{
            $otp = mt_rand(100000, 999999);
            $USER=new User();
            $USER->user_fname=$request->user_fname;
            $USER->user_mname=$request->user_mname;
            $USER->user_lname=$request->user_lname;
            $USER->user_email=$request->user_email;
            $USER->user_phone=$request->user_phone;
            $USER->user_password=$request->user_password;
            $USER->user_dob=$request->user_dob;
            $USER->user_address_line_one=$request->user_address_line_one;
            $USER->user_address_line_two=$request->user_address_line_two;
            $USER->country_id=$request->country_id;
            $USER->user_country_phonecode=$request->phoneCode;
            $USER->states_id=$request->states_id;
            $USER->city_id=$request->city_id;
            $USER->user_zip_code=$request->user_zip_code;
            $USER->user_photo=$request->user_photo;
            $USER->user_platform_otp=$otp;
            if($USER->save()){
                if((int)$USER->user_id>0){
                    return response()->json(array("msg"=>"Registration Successfull","otp"=>$otp,"status"=>201),201);
                }
                else{
                    return response()->json(array("msg"=>"Registration Failed","status"=>501),501);
                }
            }
            else{
                return response()->json(array("msg"=>"Registration Failed","status"=>501),501);
            }
        }
        catch (Exception $e) {
            return response()->json(array("msg"=>"Bad Request","data"=>$e,"status"=>400),400);
        }
    }
    public function login(Request $request){
        try{
            $otp=0;
            $user_email=$request->user_email;
            $password=$request->user_password;
            $user=User::where('user_email', $user_email)->where('user_password', $password)->get();
            if(count($user)===0){
                return response()->json(array("msg"=>"Credentials did not match. Please Try again.","data"=>[],"status"=>401),401);
            }
            $userArray=json_decode($user,true);
            $msg="Please verify your email id. OTP sent to your registered email id";
            $status=403;
            if(array_key_exists(0,$userArray)){
                if(array_key_exists("user_platform_verification_status",$userArray[0])){
                    if((int)$userArray[0]['user_platform_verification_status']===1){
                        $msg="Login successfully";
                        $status=200;
                    }
                }

            }
            if($status===403){
                $otp = mt_rand(100000, 999999);
                $affectedRows = User::where('user_email', $user_email)->update(array('user_platform_otp' => $otp));
            }
            return response()->json(array("msg"=>$msg,"data"=>$user,"otp"=>$otp,"status"=>$status),$status);
        }
        catch (Exception $e) {
            return response()->json(array("msg"=>"Bad Request","status"=>400),400);
        }
    }
    public function profile(Request $request){
        try{
            $id=$request->user_id;
            $user=User::find($id);
            if(is_null($user)){
                return response()->json(array("msg"=>"Record Not Found","data"=>$request->user_id,"status"=>404),404);
            }
            return response()->json(array("msg"=>"Record Found","data"=>$user,"status"=>200),200);
        }
        catch (Exception $e) {
            return response()->json(array("msg"=>"Bad Request","status"=>400),400);
        }
    }
    public function forgetpass(Request $request){
        try{
            $user_email=$request->user_email;
            $user=User::where('user_email', $user_email)->get();
            if(count($user)===0){
                return response()->json(array("msg"=>"Email do not exist","data"=>[],"status"=>404),404);
            }
            $otp = mt_rand(100000, 999999);
            $affectedRows = User::where('user_email', $user_email)->update(array('user_platform_otp' => $otp));
            if($affectedRows>0){
                return response()->json(array("msg"=>"Send OTO","data"=>$user,"otp"=>$otp,"status"=>200),200);
            }
            else
            return response()->json(array("msg"=>"Email do not exist","data"=>[],"status"=>404),404);
        }
        catch (Exception $e) {
            return response()->json(array("msg"=>"Bad Request","data"=>[],"status"=>400),400);
        }
    }
    public function forgetpassSave(Request $request){
        try{
            $user_email=$request->user_email;
            $user_platform_otp=$request->user_platform_otp;
            $user_password=$request->user_password;
            $user_platform_otp=$request->user_platform_otp;
            // check otp exist with time limit
            $otpValidationResult=$this->checkValidOtp($user_email,$user_platform_otp);

            $msg="OTP entered is invalid";
            $status=403;
            //exit;
            switch($otpValidationResult["txt_status"]){
                case "OK":
                    $msg="Password is required.";
                    $status=404;
                    if(strlen(trim($user_password))>0){
                        $affectedRows = User::where('user_email', $user_email)->where('user_platform_otp', $user_platform_otp)->update(array('user_password' => MD5($user_password),"user_platform_otp"=>""));
                        if($affectedRows>0){
                            $msg="Password successfully changed.";
                            $status=201;
                        }
                    }
                break;
                case "Invalid":
                    $msg="OTP entered is invalid";
                    $status=403;
                break;
                case "TimeOut":
                    $status=400;
                    $msg="OTP is timeout";
                break;
            }
            return response()->json(array("msg"=>$msg,"data"=>[],"status"=>$status),$status);
        }
        catch (Exception $e) {
            return response()->json(array("msg"=>"Bad Request","data"=>[],"status"=>400),400);
        }
    }

    public function changepassword(Request $request){
        $user_id=$request->user_id;
        $old_password=$request->old_password;
        $new_password=$request->new_password;
        $msg="Your old password did not match. Please try again.";
        $status=403;
        $user=User::where('user_id', $user_id)->where('user_password', $old_password)->get();
        if(!empty($user)){
            if(count($user)>0){
                $affectedRows = User::where('user_id', $user_id)->where('user_password', $old_password)->update(array('user_password' => $new_password,"user_platform_otp"=>""));
                $msg="Your Password is modified successfully";
                $status=201;
            }
        }
        else{
            $msg="Your old password did not match. Please try again.";
            $status=401;
        }

       return response()->json(array("msg"=>$msg,"data"=>[],"status"=>$status),$status);
    }

    public function otpveificationemail(Request $request){
        try{
            $user_email=$request->user_email;
            $user_platform_otp=$request->user_platform_otp;
            // check otp exist with time limit
            $otpValidationResult=$this->checkValidOtp($user_email,$user_platform_otp);
            $msg="OTP entered is invalid";
            $status=401;
            //exit;
            switch($otpValidationResult["txt_status"]){
                case "OK":
                    $msg="Email ID is not Exist.";
                    $status=404;
                    if(strlen(trim($user_email))>0){
                        $affectedRows = User::where('user_email', $user_email)->where('user_platform_otp', $user_platform_otp)->update(array('user_platform_verification_status' => 1,"user_platform_otp"=>""));
                        if($affectedRows>0){
                            $msg="Your account is activated. Now you can login using your credentials";
                            $status=201;
                        }
                    }
                break;
                case "Invalid":
                    $msg="OTP entered is invalid";
                    $status=401;
                break;
                case "TimeOut":
                    $status=400;
                    $msg="OTP is timeout";
                break;
            }
            return response()->json(array("msg"=>$msg,"data"=>[],"status"=>$status),$status);
        }
        catch (Exception $e) {
            return response()->json(array("msg"=>"Bad Request","data"=>[],"status"=>400),400);
        }
    }

    public function saveBank(Request $request){
        $fundingAccount=new FundingAccount;
        $fundingAccount->account_name=$request->account_name;
        $fundingAccount->last_four=$request->last_four;
        $fundingAccount->nickname=$request->nickname;
        $fundingAccount->state=$request->state;
        $fundingAccount->token=$request->token;
        $fundingAccount->type=$request->type;
        $fundingAccount->source_type=$request->source_type;
        $fundingAccount->scouce_name=$request->scouce_name;
        $fundingAccount->user_id=$request->user_id;
        $fundingAccount->save();
        return response()->json(array("msg"=>"Record Found","data"=>[],"status"=>200),200);

    }
    public function getBank(Request $request){
        try{
            $id=$request->user_id;
            $user=FundingAccount::select("funding_id","account_name","last_four", "source_type as source_type", "scouce_name as bank_name")->where("user_id",$id)->get();//find($id);
            if(is_null($user)){
                return response()->json(array("msg"=>"Record Not Found","data"=>$request->user_id,"status"=>404),404);
            }
            return response()->json(array("msg"=>"Record Found","data"=>$user,"status"=>200),200);
        }
        catch (Exception $e) {
            return response()->json(array("msg"=>"Bad Request","status"=>400),400);
        }

    }



}
