<?php

namespace App\Model;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property integer $user_id
 * @property string $user_fname
 * @property string $user_mname
 * @property string $user_lname
 * @property string $user_email
 * @property string $user_phone
 * @property string $user_password
 * @property string $user_photo
 * @property string $user_dob
 * @property string $user_address_line_one
 * @property string $user_address_line_two
 * @property int $mst_country_id
 * @property int $mst_state_id
 * @property int $mst_city_id
 * @property string $user_zip_code
 * @property string $user_created_on
 * @property string $user_updated_on
 * @property int $user_platform_verification_status
 * @property string $user_platform_otp
 * @property int $user_synapse_status
 * @property string $user_country_phonecode
 */
class User extends Authenticatable
{
    use Notifiable, HasApiTokens;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = '_users';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'user_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['user_fname', 'user_mname', 'user_lname', 'user_email', 'user_country_phonecode','user_phone', 'user_password', 'user_photo', 'user_dob', 'user_address_line_one', 'user_address_line_two', 'mst_country_id', 'mst_state_id', 'mst_city_id', 'user_zip_code', 'created_at', 'updated_at'];

}
