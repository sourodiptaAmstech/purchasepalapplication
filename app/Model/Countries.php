<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table="_countries";

     /**
     * @var array
     */
    protected $fillable=['name','sortname','phonecode'];

    public function states()
    {
        return $this->hasMany('App\Models\States');
    }

}
