<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property int $state_id
 */
class Cities extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = '_cities';

    /**
     * @var array
     */
    protected $fillable = ['name', 'state_id'];

    public function state()
    {
        return $this->belongsTo('App\Models\States');
    }

}
