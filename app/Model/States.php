<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property int $country_id
 */
class States extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = '_states';

    /**
     * @var array
     */
    protected $fillable = ['name', 'country_id'];

    public function country()
    {
        return $this->belongsTo('App\Models\Countries');
    }
    public function city()
    {
        return $this->hasMany('App\Models\Cities');
    }

}
