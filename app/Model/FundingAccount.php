<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property int $country_id
 */
class FundingAccount extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = '_funding_account';
    protected $primaryKey = 'funding_id';

    

}
